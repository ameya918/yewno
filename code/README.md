The script parses the whole blog corpus as per age group as well as capability for all blogs combined.

The output is stored in output.md

1. Please provide an evaluation of the scalability of your solution (in terms of actual algorithm,

infrastructure, or both)?
The current parsing can be modified to  distibute among different nodes for different age groups on a distributed network. with intermediate results been written to system disk or hdfs. (the code has been commented for count map been written to system disk)

2. Suppose working in a production environment where new documents are constantly added to

the corpus. Discuss the applicability of your solution and outline potential improvements to the

core algorithm - if any.

With the logic which is commented to select only files after last modified timestamp of count_map file we would be extract/parse only new documents which are updated or added later.

