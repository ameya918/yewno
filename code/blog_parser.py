import math
import pickle
import os
from pprint import pprint
from time import time
from yewno_utils import *

class BlogParser(object):

    def __init__(self, path, group):
        
        self.total_posts = 0
        self.posts_with_word = {}

        fi = filename_iterator(path, group=group)
        #time_last_run = os.path.getmtime('count_map.txt')
        #print 'Time Last Run:', time_last_run
        for filename in fi:
            
            #if os.path.getmtime(filename) <= time_last_run :
                #continue
            
            # Parse all the files
            for post in get_blog_posts(filename):
                self.total_posts += 1

                tokens = set(get_tokens_for_post(post))
                for token in tokens:
                    self.posts_with_word.setdefault(token, set())
                    self.posts_with_word[token].add(self.total_posts)
    
        self.total_posts = float(self.total_posts)
        #with open('total_posts.txt', 'w') as f:
            #print >>f, self.total_posts
        #reader = open('count_map,txt', 'rb')
        #self.counts = pickle.load(reader)
        self.counts = {}
        for key in self.posts_with_word:
            self.counts[key] = len(self.posts_with_word[key])

        #output = open('count_map.txt', 'ab+')
        #pickle.dump(self.counts, output)
        #output.close()

    def get_similarity_score(self, x, y):

        # Both x & y
        pxy = len(self.posts_with_word[x] & self.posts_with_word[y]) / self.total_posts
        px = self.counts[x] / self.total_posts
        py = self.counts[y] / self.total_posts
        pp = self.calculate_score(pxy, px, py) if pxy > 0 else 0
        
        # Neither x nor y
        pxy = (self.total_posts -
               self.counts[x] -
               self.counts[y] +
               len(self.posts_with_word[x] & self.posts_with_word[y])) / self.total_posts
        px = (self.total_posts - self.counts[x]) / self.total_posts
        py = (self.total_posts - self.counts[y]) / self.total_posts
        nn = self.calculate_score(pxy, px, py) if pxy > 0 else 0

        # Only x
        pxy = len(self.posts_with_word[x] - self.posts_with_word[y]) / self.total_posts
        px = self.counts[x] / self.total_posts
        py = (self.total_posts - self.counts[y]) / self.total_posts
        pn = self.calculate_score(pxy, px, py) if pxy > 0 else 0

        # Only y
        pxy = len(self.posts_with_word[y] - self.posts_with_word[x]) / self.total_posts
        px = (self.total_posts - self.counts[x]) / self.total_posts
        py = self.counts[y] / self.total_posts
        np = self.calculate_score(pxy, px, py) if pxy > 0 else 0



        return pp + pn + np + nn

    def get_similar_words(self, search_key, num_words):
        """
            Function to compute similar words to search_key limited by num_words
        """
        similarity_scores = {}
        for key in self.posts_with_word:
            if key != search_key:
                similarity_scores[key] = self.get_similarity_score(search_key, key)

        return sorted(similarity_scores.items(), key=lambda x: x[1], reverse=True)[0:num_words]

    @staticmethod
    def calculate_score(pxy, px, py):
        return pxy * math.log(pxy / (px * py))




if __name__ == "__main__":
    
    
    search_key = 'airport'
    limit = 5

    print "10s blogs"
    timestamp = time()
    bp = BlogParser(path="./../blogs", group='10s')
    pprint(bp.get_similar_words(search_key, limit))
    print "Time: ", time() - timestamp

    print "20s blogs"
    timestamp = time()
    bp = BlogParser(path="./../blogs", group='20s')
    pprint(bp.get_similar_words(search_key, limit))
    print "Time: ", time() - timestamp

    print "30s blogs"
    timestamp = time()
    bp = BlogParser(path="./../blogs", group='30s')
    pprint(bp.get_similar_words(search_key, limit))
    print "Time: ", time() - timestamp

    print "All blogs"
    timestamp = time()
    bp = BlogParser(path="./../blogs", group='all')
    pprint(bp.get_similar_words(search_key, limit))
    print "Time: ", time() - timestamp
