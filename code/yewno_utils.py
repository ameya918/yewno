import os
import string
from stop_words import get_stop_words
from lxml import etree


token_blacklist = None

def filename_iterator(path, group="all"):
    """
        Function to iterator over the files depending on the group
    """
    min_age = 0
    max_age = 100
    
    if group == "10s":
        min_age = 10
        max_age = 20
    
    elif group == "20s":
        min_age = 20
        max_age = 30
    
    elif group == "30s":
        min_age = 30
        max_age = 50

    for fname in [fname for fname in os.listdir(path)]:
        name_list = fname.split('.')
        if(len(name_list) > 2):
            if min_age < int(name_list[2]) < max_age:
                yield os.path.join(path, fname)

def get_blog_posts(fname):
    """
        Function to extract post from xml
    """
    file_handle = open(name=fname, mode='r')
    string = file_handle.read()
    unicode_string = unicode(string, errors="ignore")
    
    parser = etree.XMLParser(encoding='utf-8', recover=True)
    
    tree = etree.fromstring(unicode_string, parser)             
    
    for post in tree.findall('post'):
        yield post.text


def get_tokens_for_post(post):
    """
        Function to extract tokens for a blog post
    """
    global token_blacklist
    if token_blacklist is None:
        token_blacklist = set(get_stop_words('english'))
    
    return [x.strip(string.punctuation).lower() for x in post.split() if x not in token_blacklist]